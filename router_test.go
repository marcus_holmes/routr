package routr

import (
	"github.com/gorilla/mux"
	. "bitbucket.com/marcus_holmes/TraceLog"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
)

var testResponse string = "test response"

func TestServer(t *testing.T) {
	Trace(TLTest, 1, "Testing Router on loopback Server")

	route := "/test"
	pr := getRouteTestPage(route)
	pr.AddToList()
	apiroute := "/testapi"
	ar := getRouteTestApi(apiroute, "POST", t)
	ar.AddToList()
	rt := SetUp("")
	srv := httptest.NewServer(rt)
	defer srv.Close()
	res, err := http.Get(srv.URL + route)
	if err != nil {
		t.Fatal("test server didn't handle the GET because: " + err.Error())
	}
	bar, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		t.Fatal("error closing response body: " + err.Error())
	}
	response := string(bar)
	if response != testResponse {
		t.Fatal("Wrong GET Response! expecting: " + testResponse + " got: '" + response + "'")
	}

	vs := make(url.Values)
	vs.Set("testvalue", testResponse)
	res, err = http.PostForm(srv.URL+apiroute, vs)
	bar, err = ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		t.Fatal("error closing response body: " + err.Error())
	}
	response = string(bar)
	if response != testResponse {
		t.Fatal("Wrong POST Response! expecting: " + testResponse + " got: '" + response + "'")
	}
}

func getRouteTestApi(route string, method string, t *testing.T) ApiRegistration {
	apiHandler := func(w http.ResponseWriter, r *http.Request) {
		err := r.ParseForm()
		if nil != err {
			t.Fatal("error parsing form values in api call: " + err.Error())
		}
		retval := r.FormValue("testvalue")
		w.Write([]byte(retval))
	}

	ar := new(ApiRegistration)
	ar.Name = "testResponseApi"
	ar.RegisterFunc = func(rt *mux.Router) error {
		Trace(TLInfo, 5, "registering test API route")
		rt.HandleFunc(route, apiHandler).Methods(method)
		return nil
	}
	return *ar
}

func getRouteTestPage(route string) PageRegistration {
	pr := new(PageRegistration)
	pr.Name = "testResponsePage"
	pr.RegisterFunc = func(rt *mux.Router) error {
		Trace(TLInfo, 5, "registering test Page route")
		rt.HandleFunc(route, pageHandler).Methods("GET")
		return nil
	}
	pr.CompileTemplate = func(pr *PageRegistration) error {
		Trace(TLInfo, 5, "compiling test templates")
		return nil
	}
	return *pr
}

func pageHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("test response"))
}
