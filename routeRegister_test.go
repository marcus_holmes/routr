package routr

import (
	"github.com/gorilla/mux"
	. "bitbucket.com/marcus_holmes/TraceLog"
	"testing"
)

func TestPageRegister(t *testing.T) {
	Trace(TLTest, 1, "Testing Route Register with Page")
	pr := getTestPage()
	pr.AddToList()
}

func TestApiRegister(t *testing.T) {
	Trace(TLTest, 1, "Testing Route Register with Api")
	ar := getTestApi()
	ar.AddToList()
}
func TestRouteComposition(t *testing.T) {
	var rr RouteRegistration
	rr = getTestPage().RouteRegistration
	Trace(TLTest, 1, "Conversion from PageReg succeeded: "+rr.Name)
	rr = getTestApi().RouteRegistration
	Trace(TLTest, 1, "Conversion from ApiReg succeeded: "+rr.Name)
}

func getTestApi() ApiRegistration {
	ar := new(ApiRegistration)
	ar.Name = "testapi"
	ar.RegisterFunc = func(rt *mux.Router) error {
		Trace(TLInfo, 5, "registering test route")
		return nil
	}
	return *ar
}

func getTestPage() PageRegistration {
	pr := new(PageRegistration)
	pr.Name = "testpage"
	pr.RegisterFunc = func(rt *mux.Router) error {
		Trace(TLInfo, 5, "registering test route")
		return nil
	}
	pr.CompileTemplate = func(pr *PageRegistration) error {
		Trace(TLInfo, 5, "compiling test templates")
		return nil
	}
	return *pr
}
