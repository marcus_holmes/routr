package routr

import (
	"errors"
	"github.com/gorilla/mux"
	. "bitbucket.com/marcus_holmes/TraceLog"
	"html/template"
)

type RegistrationFunc func(*mux.Router) error

type RouteRegistration struct {
	Name         string
	RegisterFunc RegistrationFunc
}

type ApiRegistration struct {
	RouteRegistration
}

type StaticRegistration struct {
	RouteRegistration
}

type CompileFunc func(*PageRegistration) error

type PageRegistration struct {
	RouteRegistration
	CompileTemplate CompileFunc
	Compiled        bool
	Template        *template.Template
}

func (rr *RouteRegistration) AddToList() {
	err := routeList.AddRoute(*rr)
	if err != nil {
		Trace(TLError, 1, "failed to add route "+rr.Name+" to the route list because: "+err.Error())
	}
}

type RouteList struct {
	Routes map[string]RouteRegistration
}

func (rl *RouteList) RouteCount() int {
	return len(rl.Routes)
}

func (rl *RouteList) AddRoute(rr RouteRegistration) error {
	if _, ok := rl.Routes[rr.Name]; ok {
		// key already present, they've got to delete it first
		return errors.New("Route " + rr.Name + " is already listed.")
	}
	rl.Routes[rr.Name] = rr
	return nil
}

func (rl *RouteList) RemoveRoute(rn string) error {
	if _, ok := rl.Routes[rn]; ok {
		// key present, good to go
		delete(rl.Routes, rn)
		return nil
	}
	//page not there, so no need to delete
	return nil
}

func NewRouteList() RouteList {
	nrl := new(RouteList)
	nrl.Routes = make(map[string]RouteRegistration)
	return *nrl
}

var routeList = NewRouteList()
