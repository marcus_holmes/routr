package routr

import (
	"github.com/gorilla/mux"
	. "bitbucket.com/marcus_holmes/TraceLog"
	"net/http"
)

func SetUp() *mux.Router {
	rt := mux.NewRouter()
	registerRoutes(rt)
	//should check to see if "/" is registered as a route and register the default if not
	//rt.HandleFunc("/", defaultHandler)
	rt.NotFoundHandler = http.HandlerFunc(page404Handler)
	return rt
}

func RegisterRoute(rt *mux.Router, rr RouteRegistration) error {
	return rr.RegisterFunc(rt)
}

func registerRoutes(rt *mux.Router) {
	// call all the page registration functions to register the pages in the router
	var err error
	Trace(TLInfo, 1, "Registering Routes...")
	for _, rl := range routeList.Routes {
		err = RegisterRoute(rt, rl)
		if err != nil {
			Trace(TLError, 1, "   Route "+rl.Name+" failed to register successfully")
		} else {
			Trace(TLInfo, 1, "   Route "+rl.Name+" registered successfully")
		}
	}
	Trace(TLInfo, 1, "...complete")
}

func defaultHandler(w http.ResponseWriter, r *http.Request) {
	Trace(TLInfo, 5, "Received connection request on "+r.Host+" from "+r.RemoteAddr)
	Trace(TLInfo, 5, "...returning default page handler")
	w.Header().Set("Content-Type", "text/plain")
	//check if there's a default template set and return that if so.

	// if not, let's just send some text
	w.Write([]byte("This site is still in development. This is the default page content for a page that will exist in the future, but doesn't yet. We're working on it!\n"))
}

func page404Handler(w http.ResponseWriter, r *http.Request) {
	Trace(TLInfo, 2, "Received connection request on "+r.Host+" from "+r.RemoteAddr)
	Trace(TLError, 2, "...returning 404 page")
	http.Error(w, "Sorry, that page doesn't seem to exist (yet). The site is still in development, so this might happen a lot", http.StatusNotFound)
}

func redirectToHTTPSHandler(w http.ResponseWriter, r *http.Request) {
	Trace(TLInfo, 5, "Received insecure connection request on "+r.URL.Scheme+r.URL.Host+r.URL.Path+" from "+r.RemoteAddr)
	Url := *r.URL
	Url.Scheme = "https"
	Url.Host = r.Host
	Trace(TLInfo, 5, "Redirecting to "+Url.String())
	http.Redirect(w, r, Url.String(), http.StatusMovedPermanently)
}
